# Improve Ci: notes


## Use Workspace
  - Already used but is it done well? Probably yeah

## Remove unused dependencies

### Unused dependencies

- [cargo udeps](https://github.com/est31/cargo-udeps)
```bash
cargo install cargo-udeps && cargo +nightly udeps
```

### Outdated dependencies

```bash
cargo outdated -wR
```

### Duplicated dependencies

```bash
cargo tree --duplicate
```

## Replace heavy dependencies

Probably the most complicated, but most rewarding in terms of compile time

```bash
cargo tree --duplicate
```


- Using serde? Check out miniserde and maybe even nanoserde.
- reqwests is quite heavy. Maybe try attohttpc or ureq, which are more lightweight.
- Swap out clap with pico-args if you only need basic argument parsing.


## Combine Integration Tests into a single binary

- Not bad if we have a lot of integration test (we don't), downside is a lot of private things would be turned to pub. Not a fan

## Disable Unused Features Of Crate Dependencies

- Doesn't always work, but sometimes it can help
- [cargo feature-set](https://github.com/badboy/cargo-feature-set)


## Use Ramdisk for compilation


- only if a lot of ram
  - [info](https://www.reddit.com/r/rust/comments/chqu4c/building_a_computer_for_fastest_possible_rust/ev02hqy)

```bash
mkdir -p target && \
sudo mount -t tmpfs none ./target && \
cat /proc/mounts | grep "$(pwd)" | sudo tee -a /etc/fstab
```

## Cache Dependency
- See [github](https://github.com/mozilla/sccache) and [this](https://medium.com/@edouard.oger/rust-caching-on-circleci-using-sccache-c996344f0115)
- Possibly not as good as CacheMaster, but cleaner

- [cache with docker](https://blog.mgattozzi.dev/caching-rust-docker-builds/)

## Switch RustC to CraneLift

- see [this](https://github.com/bjorn3/rustc_codegen_cranelift)

- Some very very good results but not always working with some specific features of nightly.
- [more infos](https://jason-williams.co.uk/a-possible-new-backend-for-rust)

## Switch linker for better performances

example with lld:

- Default is gnu Gold. objective of `mold` is ~12x faster than `lld` and ~50x faster than Gold. It's already way faster. By the creator of `lld`
- [see this](https://github.com/rui314/mold)

```toml
[target.x86_64-unknown-linux-gnu]
rustflags = [
    "-C", "link-arg=-fuse-ld=lld",
]
```
- lld cross platform might be a problem. Need further information. Mold seems perfect (too perfect?)

## Profile binary size

- [see this](https://github.com/RazrFalcon/cargo-bloat)

## Profile Compile Times

```bash
cargo rustc -- -Zself-profile

cargo -Z timings


cargo-llvm-lines
```

## Precompile macros to WASM

- [see this](https://github.com/dtolnay/watt)




# Links

https://endler.dev/2020/rust-compile-times/